//
// Created by Pavel on 21/09/2020.
//

#pragma once

#include <cmath>
#include <limits>
#include <random>

const double infinity = std::numeric_limits<double>::infinity();
const double PI = 3.1415926535897932385;

inline double degrees_to_radians(double degrees) noexcept
{
  return degrees * PI / 180.0;
}

inline double clamp(double x, double min, double max)
{
  if (x < min) {
    return min;
  } else if (x > max) {
    return max;
  }
  return x;
}

inline double random_double(double min = 0.0, double max = 1.0)
{
  static std::uniform_real_distribution<double> distribution(min, max);
  static std::mt19937 generator;
  return distribution(generator);
}