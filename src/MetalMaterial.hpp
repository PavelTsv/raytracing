//
// Created by Pavel on 14/11/2020.
//

#pragma once

#include "Material.hpp"
#include "VecGeometry.hpp"

namespace rt {
class MetalMaterial : public Material
{
public:
  explicit MetalMaterial(const rtm::Vec3d &a) : albedo(a) {}

private:
  bool scatter(const Ray &ray, const RayIntersection &intersection, rtm::Vec3d &attenuation, Ray &scattered) const override
  {
    const auto reflected = rtm::reflect(rtm::normalize(ray.direction), intersection.normal);
    scattered = Ray(intersection.point, reflected);
    attenuation = albedo;
    return rtm::dot(scattered.direction, intersection.normal) > 0;
  }

public:
  rtm::Vec3d albedo;
};
}// namespace rt