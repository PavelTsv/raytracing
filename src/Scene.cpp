//
// Created by Pavel on 27/09/2020.
//

#include "Scene.hpp"

std::optional<rt::RayIntersection> rt::Scene::closest_intersection(
  const rt::Ray &ray,
  double t_min,
  double t_max) const noexcept
{
  std::optional<rt::RayIntersection> intersection = std::nullopt;
  auto closest_so_far = t_max;

  for (const auto &object : renderables) {
    if (const auto tmp_intersection = object->closest_intersection(ray, t_min, closest_so_far)) {
      closest_so_far = tmp_intersection->t;
      intersection = tmp_intersection;
    }
  }

  return intersection;
}
