//
// Created by Pavel on 13/09/2020.
//

#pragma once

#include "Math.h"
#include "Vec3.hpp"
#include "VecCommon.hpp"

#include <iostream>

namespace rt {
void write_color(std::ostream &os, const rtm::Vec3<double> &v, std::size_t samples_per_pixel)
{
  const auto scale = 1.0 / samples_per_pixel;
  const auto v_scaled = 256 * rtm::clamp(v * scale, 0.0, 0.999);
  os << static_cast<int>(v_scaled.x) << ' '
     << static_cast<int>(v_scaled.y) << ' '
     << static_cast<int>(v_scaled.z) << '\n';
}
}// namespace rt