//
// Created by Pavel on 14/11/2020.
//

#pragma once

#include "Ray.hpp"
#include "Renderable.hpp"

namespace rt {

struct RayIntersection;

class Material
{
public:
  virtual ~Material() = default;
  virtual bool scatter(const Ray &ray, const RayIntersection &intersection, rtm::Vec3d &attenuation, Ray &scattered) const = 0;
};

}// namespace rt