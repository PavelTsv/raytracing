//
// Created by Pavel on 27/09/2020.
//

#pragma once

#include <utility>

#include "Renderable.hpp"
#include "VecGeometry.hpp"

namespace rt {

class Sphere : public Renderable
{
public:
  Sphere() = default;
  Sphere(
    const rtm::Vec3d &cen,
    double r,
    std::shared_ptr<Material> m) : center{ cen }, radius{ r }, material{ std::move(m) }
  {
  }

  [[nodiscard]] std::optional<RayIntersection> closest_intersection(
    const Ray &ray,
    double t_min,
    double t_max) const noexcept override
  {
    const auto oc = ray.origin - center;
    const auto a = rtm::length_squared(ray.direction);
    const auto half_b = rtm::dot(oc, ray.direction);
    const auto c = rtm::length_squared(oc) - radius * radius;
    const auto disc = half_b * half_b - a * c;

    if (disc > 0) {
      const auto root = std::sqrt(disc);
      auto t = (-half_b - root) / a;
      if (t < t_max && t > t_min) {
        const auto intersection_point = ray.at(t);
        const auto outward_normal = (intersection_point - center) / radius;
        const auto face_same_direction = rtm::dot(ray.direction, outward_normal) < 0;
        const auto normal = face_same_direction ? outward_normal : -outward_normal;
        return std::optional<RayIntersection>{ std::in_place, intersection_point, normal, material, t };
      }

      t = (-half_b + root) / a;
      if (t < t_max && t > t_min) {
        const auto intersection_point = ray.at(t);
        const auto normal = (intersection_point - center) / radius;
        return std::optional<RayIntersection>{ std::in_place, intersection_point, normal, material, t };
      }
    }
    return std::nullopt;
  }

private:
  rtm::Vec3d center{};
  double radius{};
  std::shared_ptr<Material> material;
};
}// namespace rt
