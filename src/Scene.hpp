//
// Created by Pavel on 27/09/2020.
//

#pragma once

#include "Renderable.hpp"
#include <memory>
#include <vector>

namespace rt {
class Scene
{
public:
  void add_renderable(const std::shared_ptr<Renderable> &renderable)
  {
    renderables.push_back(renderable);
  }

  [[nodiscard]] std::optional<RayIntersection> closest_intersection(
    const Ray &ray,
    double t_min,
    double t_max) const noexcept;

private:
  std::vector<std::shared_ptr<Renderable>> renderables;
};
}// namespace rt