//
// Created by Pavel on 08/11/2020.
//

#pragma once


#include "Ray.hpp"
#include "Vec3.hpp"

namespace rt {
class Camera
{
public:
  Camera(
    double aspect_ratio,
    double viewport_height,
    double focal_length) noexcept : origin{},
                                    horizontal{ aspect_ratio * viewport_height, 0, 0 },
                                    vertical{ 0, viewport_height, 0 }
  {
    lower_left_corner = origin - horizontal / 2 - vertical / 2 - rtm::Vec3d{ 0, 0, focal_length };
  }

  [[nodiscard]] inline rt::Ray trace_ray(double u, double v) const noexcept
  {
    return { origin, lower_left_corner + u * horizontal + v * vertical - origin };
  }

private:
  rtm::Vec3d origin;
  rtm::Vec3d lower_left_corner{};
  rtm::Vec3d horizontal;
  rtm::Vec3d vertical;
};
}// namespace rt
