//
// Created by Pavel on 14/11/2020.
//

#pragma once

#include "Material.hpp"
#include "Math.h"

static rtm::Vec3d random_unit_vector()
{
  const double a = random_double(0, 2 * PI);
  const double z = random_double(-1, 1);
  const double r = std::sqrt(1 - z * z);

  return { r * std::cos(a), r * std::sin(a), z };
}

namespace rt {
class LambertianMaterial : public Material
{
public:
  explicit LambertianMaterial(const rtm::Vec3d a) : albedo(a) {}

  bool scatter(const Ray &ray, const RayIntersection &intersection, rtm::Vec3d &attenuation, Ray &scattered) const override
  {
    const auto scatter_dir = intersection.normal + random_unit_vector();
    scattered = Ray(intersection.point, scatter_dir);
    attenuation = albedo;
    //TODO FIX
    (void)ray;
    return true;
  }
  rtm::Vec3d albedo;
};
}// namespace rt
