//
// Created by Pavel on 13/09/2020.
//

#pragma once

#include "Vec3.hpp"
#include <cmath>

namespace rtm {
template<typename T>
constexpr T length(const Vec3<T> &v) noexcept
{
  return std::sqrt(dot(v, v));
}

template<typename T>
constexpr T length_squared(const Vec3<T> &v) noexcept
{
  return dot(v, v);
}

template<typename T>
constexpr T distance(const Vec3<T> &v1, const Vec3<T> &v2) noexcept
{
  return length(v2 - v1);
}

template<typename T>
constexpr T dot(const Vec3<T> &v1, const Vec3<T> &v2) noexcept
{
  const auto tmp = v1 * v2;
  return tmp.x + tmp.y + tmp.z;
}

template<typename T>
constexpr T cross(const Vec3<T> &v1, const Vec3<T> &v2) noexcept
{
  return { v1.y * v2.z - v2.y * v1.z,
    v1.z * v2.x - v2.z * v1.x,
    v1.x * v2.y - v2.x * v1.y };
}
template<typename T>
constexpr Vec3<T> normalize(const Vec3<T> &v)
{
  return v * static_cast<T>(1) / length(v);
}

template<typename T>
constexpr rtm::Vec3d reflect(const Vec3<T> &v1, const Vec3<T> &v2) noexcept
{
  return v1 - 2 * dot(v1, v2) * v2;
}

}// namespace rtm
