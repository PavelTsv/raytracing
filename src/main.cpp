#include "Camera.hpp"
#include "Color.hpp"
#include "LambertianMaterial.hpp"
#include "MetalMaterial.hpp"
#include "Scene.hpp"
#include "Sphere.hpp"
#include <fstream>
#include <iostream>

template<typename T>
constexpr double lerp(T v0, T v1, double t)
{
  return (1.0 - t) * v0 + t * v1;
}

rtm::Vec3d ray_color(const rt::Ray &r, const rt::Scene &world, std::size_t depth)
{
  if (depth <= 0) {
    return {};
  }
  if (const auto intersection = world.closest_intersection(r, 0.001, infinity)) {
    rt::Ray scattered;
    rtm::Vec3d attenuation;
    if (intersection->material->scatter(r, intersection.value(), attenuation, scattered)) {
      return attenuation * ray_color(scattered, world, depth - 1);
    }
    return {};
  }
  const auto unit_dir = rtm::normalize(r.direction);
  const auto t = 0.5 * (unit_dir.y + 1.0);
  return (1.0 - t) * rtm::Vec3d{ 1.0, 1.0, 1.0 } + t * rtm::Vec3d(0.5, 0.7, 1.0);
}

int main()
{
  const auto aspect_ratio = 16.0 / 9.0;
  const int image_width = 400;
  const int image_height = static_cast<int>(image_width / aspect_ratio);
  const auto viewport_height = 2.0;
  const auto focal_length = 1.0;
  const auto samples_per_pixel = 100;
  const auto max_depth = 50;

  rt::Camera camera{ aspect_ratio, viewport_height, focal_length };

  rt::Scene scene{};
  auto ground_material = std::make_shared<rt::LambertianMaterial>(rtm::Vec3d{ 0.8, 0.8, 0.0 });
  const auto center_material = std::make_shared<rt::LambertianMaterial>(rtm::Vec3d{ 0.7, 0.3, 0.3 });
  const auto left_material = std::make_shared<rt::MetalMaterial>(rtm::Vec3d{ 0.8, 0.8, 0.8 });
  const auto right_material = std::make_shared<rt::MetalMaterial>(rtm::Vec3d{ 0.8, 0.6, 0.2 });

  scene.add_renderable(std::make_shared<rt::Sphere>(rtm::Vec3d{ 0, -100.5, -1 }, 100, ground_material));
  scene.add_renderable(std::make_shared<rt::Sphere>(rtm::Vec3d{ 0, 0, -1 }, 0.5, center_material));
  scene.add_renderable(std::make_shared<rt::Sphere>(rtm::Vec3d{ -1, 0, -1 }, 0.5, left_material));
  scene.add_renderable(std::make_shared<rt::Sphere>(rtm::Vec3d{ 1, 0, -1 }, 0.5, right_material));

  std::ofstream file;
  file.open("image.ppm");
  file << "P3\n"
       << image_width << ' ' << image_height << "\n255\n";

  for (int j = image_height - 1; j >= 0; --j) {
    for (int i = 0; i < image_width; ++i) {
      rtm::Vec3d pixel_color{};
      for (int k = 0; k < samples_per_pixel; ++k) {
        const auto u = (i + random_double()) / (image_width - 1);
        const auto v = (j + random_double()) / (image_height - 1);
        const auto r = camera.trace_ray(u, v);
        pixel_color += ray_color(r, scene, max_depth);
      }
      rt::write_color(file, pixel_color, samples_per_pixel);
    }
  }
  file.close();

  return 0;
}
