//
// Created by Pavel on 22/09/2020.
//

#pragma once

#include "Material.hpp"
#include "Ray.hpp"
#include <memory>
#include <optional>
#include <utility>

namespace rt {

class Material;

struct RayIntersection
{
  RayIntersection(
    const rtm::Vec3d &p,
    const rtm::Vec3d &n,
    std::shared_ptr<Material> m,
    double delta) : point{ p }, normal{ n }, material{ std::move(m) }, t{ delta } {}
  rtm::Vec3d point;
  rtm::Vec3d normal;
  std::shared_ptr<Material> material;
  double t;
};

class Renderable
{
public:
  virtual ~Renderable() = default;
  [[nodiscard]] virtual std::optional<RayIntersection> closest_intersection(
    const Ray &ray,
    double t_min,
    double t_max) const noexcept = 0;
};
}// namespace rt