//
// Created by Pavel on 19/09/2020.
//

#pragma once

#include "Vec3.hpp"

namespace rt {
class Ray
{
public:
  constexpr Ray() = default;

  constexpr Ray(const rtm::Vec3d &orig, const rtm::Vec3d &dir) noexcept
    : origin(orig), direction(dir) {}

  [[nodiscard]] constexpr rtm::Vec3d at(double t) const noexcept
  {
    return origin + t * direction;
  }

  //TODO change back to const after changing trace function return type to Optional<Intersection>
  rtm::Vec3d origin{};
  rtm::Vec3d direction{};
};
}// namespace rt