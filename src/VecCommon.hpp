//
// Created by Pavel on 10/11/2020.
//

#pragma once

#include "Vec3.hpp"

namespace rtm {

constexpr rtm::Vec3d min(const rtm::Vec3d &v, double val) noexcept
{
  return { std::min(v.x, val), std::min(v.y, val), std::min(v.z, val) };
}

constexpr rtm::Vec3d max(const rtm::Vec3d &v, double val) noexcept
{
  return { std::max(v.x, val), std::max(v.y, val), std::max(v.z, val) };
}

constexpr rtm::Vec3d clamp(const rtm::Vec3d &v, double min_val, double max_val) noexcept
{
  return min(max(v, min_val), max_val);
}
}// namespace rtm