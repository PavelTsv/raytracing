//
// Created by Pavel on 12/09/2020.
//

#pragma once

#include "iostream"
#include <cassert>

namespace rtm {
template<typename T>
class Vec3
{
public:
  constexpr Vec3() = default;
  constexpr Vec3(const Vec3<T> &v) = default;
  constexpr explicit Vec3(T scalar) noexcept
    : x(scalar), y(scalar), z(scalar) {}
  constexpr Vec3(T _x, T _y, T _z) noexcept : x(_x), y(_y), z(_z) {}
  template<typename U>
  constexpr explicit Vec3(const Vec3<U> &v)
    : x(static_cast<T>(v.x)), y(static_cast<T>(v.y)), z(static_cast<T>(v.z))
  {
  }

  constexpr T operator[](std::size_t i) const
  {
    assert(i >= 0 && i < length);
    switch (i) {
    default:
    case 0:
      return x;
    case 1:
      return y;
    case 2:
      return z;
    }
  }

  constexpr T &operator[](std::size_t i)
  {
    assert(i >= 0 && i < length);
    switch (i) {
    default:
    case 0:
      return x;
    case 1:
      return y;
    case 2:
      return z;
    }
  }

  constexpr Vec3 &operator=(const Vec3 &v) = default;

  template<typename U>
  constexpr Vec3 &operator=(const Vec3<U> &v) noexcept
  {
    x = v.x;
    y = v.y;
    z = v.z;
  }

  constexpr Vec3 &operator+=(const Vec3 &v) noexcept
  {
    x += v.x;
    y += v.y;
    z += v.z;
    return *this;
  }

  template<typename U>
  constexpr Vec3 &operator+=(const Vec3<U> &v) noexcept
  {
    x += v.x;
    y += v.y;
    z += v.z;
    return *this;
  }

  constexpr Vec3 &operator+=(T scalar) noexcept
  {
    x += scalar;
    y += scalar;
    z += scalar;
    return *this;
  }

  constexpr Vec3 &operator-=(const Vec3 &v) noexcept
  {
    x -= v.x;
    y -= v.y;
    z -= v.z;
    return *this;
  }

  template<typename U>
  constexpr Vec3 &operator-=(const Vec3<U> &v) noexcept
  {
    x -= v.x;
    y -= v.y;
    z -= v.z;
    return *this;
  }

  constexpr Vec3 &operator-=(T scalar) noexcept
  {
    x -= scalar;
    y -= scalar;
    z -= scalar;
    return *this;
  }

  constexpr Vec3 &operator*=(const Vec3 &v) noexcept
  {
    x *= v.x;
    y *= v.y;
    z *= v.z;
    return *this;
  }

  template<typename U>
  constexpr Vec3 &operator*=(const Vec3<U> &v) noexcept
  {
    x *= v.x;
    y *= v.y;
    z *= v.z;
    return *this;
  }

  constexpr Vec3 &operator*=(T scalar) noexcept
  {
    x *= scalar;
    y *= scalar;
    z *= scalar;
    return *this;
  }

  constexpr Vec3 &operator/=(const Vec3 &v) noexcept
  {
    x /= v.x;
    y /= v.y;
    z /= v.z;
    return *this;
  }

  template<typename U>
  constexpr Vec3 &operator/=(const Vec3<U> &v) noexcept
  {
    x /= v.x;
    y /= v.y;
    z /= v.z;
    return *this;
  }

  constexpr Vec3 &operator/=(T scalar) noexcept
  {
    x /= scalar;
    y /= scalar;
    z /= scalar;
    return *this;
  }

  constexpr Vec3 operator-() const noexcept { return { -x, -y, -z }; }

  constexpr Vec3 operator+(const Vec3 &v) const noexcept
  {
    return { x + v.x, y + v.y, z + v.z };
  }

  template<typename U>
  constexpr Vec3 operator+(const Vec3<U> &v) const noexcept
  {
    return { x + v.x, y + v.y, z + v.z };
  }

  constexpr Vec3 operator+(T scalar) const noexcept
  {
    return { x + scalar, y + scalar, z + scalar };
  }

  constexpr Vec3 operator-(const Vec3 &v) const noexcept
  {
    return { x - v.x, y - v.y, z - v.z };
  }

  template<typename U>
  constexpr Vec3 operator-(const Vec3<U> &v) const noexcept
  {
    return { x - v.x, y - v.y, z - v.z };
  }

  constexpr Vec3 operator-(T scalar) const noexcept
  {
    return { x - scalar, y - scalar, z - scalar };
  }

  constexpr Vec3 operator*(const Vec3 &v) const noexcept
  {
    return { x * v.x, y * v.y, z * v.z };
  }

  template<typename U>
  constexpr Vec3 operator*(const Vec3<U> &v) const noexcept
  {
    return { x * v.x, y * v.y, z * v.z };
  }

  constexpr Vec3 operator*(T scalar) const noexcept
  {
    return { x * scalar, y * scalar, z * scalar };
  }

  constexpr Vec3 operator/(const Vec3 &v) const noexcept
  {
    return { x / v.x, y / v.y, z / v.z };
  }

  template<typename U>
  constexpr Vec3 operator/(const Vec3<U> &v) const noexcept
  {
    return { x / v.x, y / v.y, z / v.z };
  }

  constexpr Vec3 operator/(T scalar) const noexcept
  {
    return { x / scalar, y / scalar, z / scalar };
  }

  template<typename U>
  constexpr Vec3 operator*(U scalar) const noexcept
  {
    return { x * scalar, y * scalar, z * scalar };
  }

  constexpr bool operator==(const Vec3 &v) const noexcept
  {
    return x == v.x && y == v.y && z == v.z;
  }

  constexpr bool operator!=(const Vec3 &v) const noexcept
  {
    return !operator==(v);
  }

  T x, y, z;
  static const std::size_t length = 3;
};

template<typename T>
inline std::ostream &operator<<(std::ostream &os, const Vec3<T> v)
{
  return os << v.x << ' ' << v.y << ' ' << v.z;
}

template<typename T>
constexpr Vec3<T> operator*(T scalar, const Vec3<T> &v) noexcept
{
  return v * scalar;
}

template<typename T, typename U>
constexpr Vec3<T> operator*(U scalar, const Vec3<T> &v) noexcept
{
  return v * scalar;
}

using Vec3d = Vec3<double>;
using Vec3f = Vec3<float>;

}// namespace rtm
